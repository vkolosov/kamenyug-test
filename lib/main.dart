import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kamen_yug_test/bloc/ky_bloc.dart';
import 'package:kamen_yug_test/bloc/ky_state.dart';
import 'package:kamen_yug_test/data/api/fake_api.dart';
import 'package:kamen_yug_test/data/repos/api_repo.dart';
import 'package:kamen_yug_test/presentation/screens/auth_screen.dart';
import 'package:kamen_yug_test/presentation/screens/profile_screen.dart';

void main() {
  runApp(const KYApp());
}

class KYApp extends StatelessWidget {
  const KYApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'KamenYug test',
      home: BlocProvider(
        create: (_) => KYBloc(ApiRepo(fakeApi: FakeApi())),
        child: BlocBuilder<KYBloc, KYState>(
          builder: (context, state) {
            if (state is Loading) {
              return const Scaffold(
                  body: Center(child: CircularProgressIndicator()));
            }
            if (state is Authed) return ProfileScreen(login: state.login);
            return const AuthScreen();
          },
        ),
      ),
    );
  }
}
