import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kamen_yug_test/bloc/ky_event.dart';
import 'package:kamen_yug_test/bloc/ky_state.dart';
import 'package:kamen_yug_test/data/models/api_model.dart';
import 'package:kamen_yug_test/data/repos/api_repo.dart';

class KYBloc extends Bloc<KYEvent, KYState> {
  final ApiRepo apiRepo;

  KYBloc(this.apiRepo) : super(UnAuth()) {
    on<TryLoginEvent>((event, emit) async {
      emit(Loading());
      final result = await apiRepo.checkLogin(event.login, event.password);
      result
          ? emit(Authed(login: event.login, password: event.password))
          : emit(UnAuth());
    });

    on<RegisterEvent>((event, emit) async {
      emit(Loading());
      await apiRepo.addUser(UserApiModel(event.login, event.password));
      emit(Authed(login: event.login, password: event.password));
    });

    on<LogOutEvent>((event, emit) {
      emit(UnAuth());
    });
  }
}
