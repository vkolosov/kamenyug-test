abstract class KYState {}

class Loading extends KYState {}

class UnAuth extends KYState {}

class Authed extends KYState {
  final String login;
  final String password;

  Authed({required this.login, required this.password});
}
