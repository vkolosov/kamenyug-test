abstract class KYEvent {}

class TryLoginEvent extends KYEvent {
  final String login;
  final String password;

  TryLoginEvent({required this.login, required this.password});
}

class RegisterEvent extends KYEvent {
  final String login;
  final String password;

  RegisterEvent({required this.login, required this.password});
}

class LogOutEvent extends KYEvent {}
