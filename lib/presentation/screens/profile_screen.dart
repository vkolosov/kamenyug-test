import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kamen_yug_test/bloc/ky_bloc.dart';
import 'package:kamen_yug_test/bloc/ky_event.dart';

class ProfileScreen extends StatelessWidget {
  final String login;

  const ProfileScreen({Key? key, required this.login}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Profile Screen',
        ),
        centerTitle: true,
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              login,
              style: const TextStyle(fontSize: 20),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () => context.read<KYBloc>().add(LogOutEvent()),
              child: const Text('Exit'),
            ),
          ],
        ),
      ),
    );
  }
}
