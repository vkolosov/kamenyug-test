import 'package:flutter/material.dart';
import 'package:kamen_yug_test/presentation/widgets/auth_card.dart';

enum AuthMode { signup, login }

class AuthScreen extends StatelessWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Auth Screen'),
        centerTitle: true,
      ),
      body: const Center(
        child: AuthCard(),
      ),
    );
  }
}
