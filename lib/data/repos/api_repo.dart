import 'package:kamen_yug_test/data/api/fake_api.dart';
import 'package:kamen_yug_test/data/models/api_model.dart';

class ApiRepo {
  final FakeApi fakeApi;

  ApiRepo({required this.fakeApi});

  Future<bool> checkLogin(String login, String password) async {
    final usersData = await fakeApi.getData();
    return usersData.any((el) => el.login == login && el.password == password);
  }

  Future<void> addUser(UserApiModel userApiModel) async {
    await fakeApi.addUser(userApiModel);
  }
}
