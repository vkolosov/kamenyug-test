class UserApiModel {
  final String login;
  final String password;

  UserApiModel(this.login, this.password);

  UserApiModel.fromJson(Map<String, dynamic> json)
      : login = json['login'],
        password = json['password'];

  Map<String, dynamic> toJson() {
    return {
      "login": login,
      "password": password,
    };
  }
}
