import 'package:kamen_yug_test/data/models/api_model.dart';

class FakeApi {
  static List<Map<String, dynamic>> registeredUsers = [
    {"login": 'test', "password": '123'},
    {"login": 'test2', "password": '1234'},
  ];

  Future<List<UserApiModel>> getData() async {
    await Future.delayed(const Duration(seconds: 1));
    return registeredUsers.map((e) => UserApiModel.fromJson(e)).toList();
  }

  Future<void> addUser(UserApiModel userApi) async {
    await Future.delayed(const Duration(seconds: 1));
    registeredUsers.add(userApi.toJson());
  }
}
